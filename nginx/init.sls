
#########################################################
# ACL variables per ENV. Add or remove IPs just here    #
#########################################################

{% set dev_allowed_ips = [
'127.0.0.1',
'0.0.0.0/0'
]
%}


{% set proxy_headers = [
'proxy_set_header Host $host',
'proxy_set_header X-Real-IP $remote_addr',
'proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for',
'proxy_set_header X-Forwarded-Proto $scheme',
'proxy_set_header X-Forwarded-Host $host',
'proxy_set_header X-Forwarded-Server $host',
'proxy_set_header X-Forwarded-Port $server_port',
]
%}

{% set env = grains['env'] %}

ssl_certificate:
  key:
    path: /etc/ssl/private/tvtin-net.key
  cert:
    path: /etc/ssl/certs/tvtin-net.crt
  fullchain:
    path: /etc/ssl/certs/ocsp-chain.crt

nginx:
  server:
    pkgs:
      - nginx
      - httpd-tools
    nginx_conf:
      worker:
        processes: "{{ salt['grains.get']('num_cpus','auto') }}"
        connections: "1024"
    proxy_header:
      header_file_path: /etc/nginx/header.conf
      header_list: {{ proxy_headers }}
    upstreams:
      zabbix-upstream:
        servers:
{% set mine_ip_addrs = salt.saltutil.runner('mine.get',
  tgt='*zabbix-web*',
  tgt_type='compound',
  fun='mine_ip_addrs') %}
{% for minion_id, ip_addr in mine_ip_addrs.items() %}
          - "{{ ip_addr[0] }}:8080 max_fails=3 fail_timeout=10s"
{% endfor %}
        maps:
{% set mine_ip_addrs = salt.saltutil.runner('mine.get',
  tgt='*zabbix-web*',
  tgt_type='compound',
  fun='mine_ip_addrs') %}
{% for minion_id, ip_addr in mine_ip_addrs.items() %}
          - "{{ minion_id }}  {{ ip_addr[0] }}:8080 "
{% endfor %}
    sites:
      nginx_proxy_zabbix:
        type: nginx_proxy
        load_blancing: True
        ssl:
          key_file: /etc/ssl/private/tvtin-net.key
          cert_file: /etc/ssl/certs/tvtin-net.crt
          fullchain: /etc/ssl/certs/tvtin-chain.crt
        access_policy:
{% if env == 'dev' %}
          allowed_ips: {{ dev_allowed_ips }}
{% elif env == 'prod' %}
          allowed_ips: {{ prod_allowed_ips }}
{% endif %}
        proxy:
          headers_path: /etc/nginx/header.conf
          upstream_proxy_pass: zabbix-upstream
          protocol: http

        host:
          name: zabbix-{{ env }}.tvtin.tk
          listen: "443 ssl http2"
##########################################################
# HTTP and Stats Server. For port redirection to HTTPS   #
##########################################################
      #nginx_stats_server:
      #  enabled: true
      #  type: nginx_stats
      #  name: stats_server
      #  host:
      #    name: rproxy-{{ env }}-s1s.tvtin.tk
      #    listen: 8080



