# Pillar Top file
base:
  '*':
    - mine.init
    - common.init
  '*db*':
    - postgresql.init
    - postgresql.secrets
  '*zabbix*':
    - docker.init
    - docker.secrets
    - zabbix.init
    - zabbix.secrets
  '*proxy*':
    - nginx.init
    - nginx.secrets
