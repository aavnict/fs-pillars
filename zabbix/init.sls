zabbix:
  db:
{% set mine_ip_addrs = salt.saltutil.runner('mine.get',
 tgt='*db*',
 tgt_type='compound',
 fun='mine_ip_addrs') %}
{% for minion_id, ip_addr in  mine_ip_addrs.items() %}
    db_server_host: "{{ ip_addr[0] }}"
{% endfor %}
    postgres_user: 'zabbix'
    db_server_port: '5432'
    postgres_db: 'zabbix'
  zabbix_server_image:
    name: 'zabbix/zabbix-server-pgsql'
    version: 'centos-4.2.6'
  zabbix_web_image:
    name: 'zabbix/zabbix-web-nginx-pgsql'
    version: 'centos-4.2.6'
    zbx_server_name: 'tvtin-demo'
    php_tz: 'Asia/Ho_Chi_Minh'
  server:
{% set mine_ip_addrs = salt.saltutil.runner('mine.get',
 tgt='*zabbix-server*',
 tgt_type='compound',
 fun='mine_ip_addrs') %}
{% for minion_id, ip_addr in  mine_ip_addrs.items() %}
    zabbix_server_host: "{{ ip_addr[0] }}"
{% endfor %}
