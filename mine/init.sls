mine_functions:
  mine_grains_items:
    mine_function: grains.items
  mine_ip_addrs:
    mine_function: network.ip_addrs
    #cidr: 192.168.0.0/16
    cidr: 172.20.0.0/16