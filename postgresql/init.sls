postgres:
  swap_size: '2'
{% set data_dir = '/pgdata/conf' %}
{% set postgresql_version = '9.6' %}
{% set postgresql_version_terse = postgresql_version | replace('.', '') %}
  conf_dir: {{ data_dir }}
  version: {{ postgresql_version }}
  prepare_cluster_command: /usr/pgsql-{{ postgresql_version }}/bin/initdb --pgdata='{{ data_dir }}' --encoding=Unicode --xlogdir=/pglogs/xlog
  pkg_repo:
    name: pgdg{{ postgresql_version_terse }}
    humanname: PostgreSQL {{ postgresql_version }} $releasever - $basearch
    gpgcheck: 1
    gpgkey: 'https://download.postgresql.org/pub/repos/yum/RPM-GPG-KEY-PGDG-{{ postgresql_version_terse }}'
    baseurl: 'https://download.postgresql.org/pub/repos/yum/{{ postgresql_version }}/redhat/rhel-$releasever-$basearch'
  pkgs:
   - postgresql{{ postgresql_version_terse }}-server
  pkgs_extra:
   - postgresql{{ postgresql_version_terse }}-contrib
   - postgresql{{ postgresql_version_terse }}-plperl
   - postgresql{{ postgresql_version_terse }}-plpython
   - postgis24_{{ postgresql_version_terse }}
   - geos
   - gdal
   - libxml2
   - libxml2-devel
   - libxslt
   - libxslt-devel
  # Create list of pg_dirs, the owner is postgres user
  service: postgresql-{{ postgresql_version }}
  new_pg_dirs:
    - /pgdata/data
    - /pgdata/logs/maintenance/log/server
    - /pgdata/conf
    - /pgdata/index
    - /pgdata/temp
    - /pgdata/file
    - /pgdata/script
    - /pgdata/xlog/
    - /pgdata/archlog
    - /pgdata/backup
    - /pgdata/backup/backup
    - /pgdata/backup/export
    - /pgdata/backup/log

  # src must be existing
  new_pg_symlinks:
    -  { name: '/pgindex/index', target: '/pgdata/index' }
    -  { name: '/pgindex/temp', target: '/pgdata/temp' }
    -  { name: '/pglogs/file', target: '/pgdata/file' }
    -  { name: '/pglogs/script', target: '/pgdata/script' }
    -  { name: '/pglogs/backup', target: '/pgdata/backup' }
    -  { name: '/pglogs/maintenance/log/server', target: '/pgdata/logs/maintenance/log/server' }
    -  { name: '/pglogs/archlog', target: '/pgdata/archlog' }
    -  { name: '/pglogs/xlog', target: '/pgdata/xlog' }
# Settings related to the pg_hba rules
{% set postgresql_default_unix_auth_method = 'trust' %}
{% set postgresql_default_ipv4_auth_method = 'md5' %}
{% set postgresql_default_ipv6_auth_method = 'md5' %}

  # Specify default rules for pg_hba.conf. Change them only if it is really necessary.
  postgresql_pg_hba_default:
    - type: "local"
      database: "all"
      role: "postgres"
      address: ""
      method: "{{ postgresql_default_unix_auth_method }}"
      comment: "local is for Unix domain socket connections only"
    - type: "host"
      database: "all"
      role: "all"
      address: "127.0.0.1/32"
      method: "{{ postgresql_default_ipv4_auth_method }}"
      comment: "IPv4 local connections"
    - type: "host"
      database: "all"
      role: "all"
      address: "::1/128"
      method: "{{ postgresql_default_ipv6_auth_method }}"
      comment: "IPv6 local connections"
    - type: "host"
      database: "all"
      role: "all"
      address: "0.0.0.0/0"
      method: "{{ postgresql_default_ipv4_auth_method }}"
      comment: "Allow all ips can access, just for testing"


  # PostgreSQL parameters which will appears in the postgresql.conf.
  postgressql_conf:
    shared_buffers: "1GB"
    max_connections: "50"
    temp_buffers: "80MB"
    work_mem: "1024MB"
    maintenance_work_mem: "16384MB"
    max_wal_size: "30GB"
    min_wal_size: "15GB"
    effective_cache_size: "48GB"

  # Create DB and user for zabbix
  users:
    zabbix:
      createdb: False

  database:
    zabbix:
      owner: 'zabbix'
