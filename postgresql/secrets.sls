#!yaml|gpg
postgres:
  postgres_admin_password: |
    -----BEGIN PGP MESSAGE-----
    Version: GnuPG v1

    hQEMA86rrGwfdO21AQf/eRZWnNlcIMxhfsZTJigJSUvMGtFXrDmZASjJmja/GW97
    F14gOXZODHsBuzujXWMt5UJvckmAc1OdcOK9yaB8yeEC4jpuczuhyiXoxWyYyb4c
    EUEMW5neRYSZZwINME0+gUrhSnmIOkmPmJP5bMKI1NR+2aL5QSYfw3ae8nUMJN3m
    UFcwUi34Ng2ihYgs2CYMuhRi+slmsQCtTKs01FOfgmwFEPdI3fGyvBytukl3CaEy
    lt1X/gGXyTkBUxjBQOhPdUGSmiBUcpJ6jYI/4mESNyNmS+i0UaJWDxjOgY7Qhwef
    TV5uywem2F2oe8KID1XX2tQw/PRW7OcUUE23rGxXZ9JEAULmHsA9COQ8rxZ6upOG
    HvBHmS411o+3QIz4RBDxRfw/iQSdvLlsHXXBK6nG76lBpzVOweVJY/4ZrKtpTAc5
    4DzRJrI=
    =Jk55
    -----END PGP MESSAGE-----
  users:
   zabbix:
    password: |
     -----BEGIN PGP MESSAGE-----
     Version: GnuPG v1

     hQEMA86rrGwfdO21AQf/eRZWnNlcIMxhfsZTJigJSUvMGtFXrDmZASjJmja/GW97
     F14gOXZODHsBuzujXWMt5UJvckmAc1OdcOK9yaB8yeEC4jpuczuhyiXoxWyYyb4c
     EUEMW5neRYSZZwINME0+gUrhSnmIOkmPmJP5bMKI1NR+2aL5QSYfw3ae8nUMJN3m
     UFcwUi34Ng2ihYgs2CYMuhRi+slmsQCtTKs01FOfgmwFEPdI3fGyvBytukl3CaEy
     lt1X/gGXyTkBUxjBQOhPdUGSmiBUcpJ6jYI/4mESNyNmS+i0UaJWDxjOgY7Qhwef
     TV5uywem2F2oe8KID1XX2tQw/PRW7OcUUE23rGxXZ9JEAULmHsA9COQ8rxZ6upOG
     HvBHmS411o+3QIz4RBDxRfw/iQSdvLlsHXXBK6nG76lBpzVOweVJY/4ZrKtpTAc5
     4DzRJrI=
     =Jk55
     -----END PGP MESSAGE-----

# Tintran65