common:
  time_zone: 'Asia/Ho_Chi_Minh'

  # vars for packages.yml
  update_exclude_packages: docker*

  pkgs:
   basic:
      - epel-release
      - redhat-lsb-core
      - vim-enhanced
      - git
      - sudo
      - wget
      - yum-cron
   utils:
      - htop
      - bind-utils
      - net-tools
      - nmap
      - s3cmd
      - python2-pip
      - python2-psutil
      - traceroute
      - jq

  zabbix_agent:
   version_repo: '4.2'
   gpgkey: 'https://repo.zabbix.com/RPM-GPG-KEY-ZABBIX-A14FE591'